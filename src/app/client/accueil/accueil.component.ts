import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Encounter, EncounterImpl } from 'src/app/models/encounter.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { EncounterService } from 'src/app/services/encounter.service';
import { TeamService } from 'src/app/services/team.service';
import { BetEntorcounterComponent } from '../modals/bet-encounter/bet-encounter.component';
import { Bettor, BettorImpl } from 'src/app/models/bettor.model';
import { BettorService } from 'src/app/services/bettor.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  encounters: Encounter[] = [];
  teams: Team[] = [];
  bettor!: Bettor;
  showFuture: boolean = false;
  buttonText: string = 'Voir matchs passés';
  alertText: string = 'Matchs à venir';
  iconClass: string = 'fa-solid fa-hourglass';

  // a supprimer une fois l'identification intégrée
  idUser: number = 3;

  constructor(
    private encounterService: EncounterService,
    private teamService: TeamService,
    private bettorService: BettorService,
    private modalService: NgbModal,
  ) { }


  ngOnInit(): void {
    //  recuperation des données match
    this.encounterService.getAllEncounter().subscribe((res) => {
      res.forEach((e) => {
        this.encounters.push(new EncounterImpl(e));
      });
      //tri par date du plus ancien au plus récent
      this.encounters.sort((a, b) => {
        let dateA = new Date(a.encounterDate);
        let dateB = new Date(b.encounterDate);
        return dateA.getTime() - dateB.getTime();
      });
    });

    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe((res) => {
      res.forEach((e) => {
        this.teams.push(new TeamImpl(e));
      });
    });

    // connexion parieur 
    this.bettorService.getBettor(this.idUser).subscribe((res) => {
      this.bettor = res;
    });

  }

  /**
   * Verifie si la date du match est aujourd'hui (pour l'afficher en vert dans la liste des matchs a venir)
   * @param dateString
   * @returns
   */
  isToday(dateString: Date): boolean {
    let date = new Date(dateString);
    let today = new Date();

    return (
      date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    );
  }
  // permet de passer de match passés a match futur
  toggleEncounter() {
    this.showFuture = !this.showFuture;
    this.buttonText = this.showFuture ? 'Voir matchs à venir' : 'Voir matchs passés';
    this.alertText = this.showFuture ? 'Matchs passés' : 'Matchs à venir';
    this.iconClass = this.showFuture ? 'fa-solid fa-hourglass-end' : 'fa-solid fa-hourglass';
  }
  betEncounter(encounter: Encounter) {
    const modalRef = this.modalService.open(BetEntorcounterComponent, {
      centered: true,
      size: 'lg',
    });
    modalRef.componentInstance.encounter = encounter;
    modalRef.componentInstance.bettor = this.bettor;
  }

}
