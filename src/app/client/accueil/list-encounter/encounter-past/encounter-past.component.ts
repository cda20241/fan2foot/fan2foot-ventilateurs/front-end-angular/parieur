import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Encounter, EncounterImpl } from 'src/app/models/encounter.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { EncounterService } from 'src/app/services/encounter.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-encounter-past',
  templateUrl: './encounter-past.component.html',
  styleUrls: ['./encounter-past.component.scss']
})
export class EncounterPastComponent implements OnInit{
  encounters: Encounter[] = [];
  teams: Team[] = [];
  

  constructor(
    private encounterService: EncounterService,
    private teamService: TeamService,
  ) {}

  ngOnInit(): void {
    //  recuperation des données match
    this.encounterService.getAllEncounter().subscribe((res) => {
      res.forEach((e) => {
        this.encounters.push(new EncounterImpl(e));
      });
      //tri par date du plus récent au plus ancien
      this.encounters.sort((a, b) => {
        let dateA = new Date(a.encounterDate);
        let dateB = new Date(b.encounterDate);
        return dateB.getTime() - dateA.getTime();
      });
    });

    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe((res) => {
      res.forEach((e) => {
        this.teams.push(new TeamImpl(e));
      });
    });
  }
  /**
   * return true si les encounters ne sont pas encore passés
   * @param date
   * @returns
   */
  isPast(date: Date): boolean {
    let today = new Date();
    let dateEncounter = new Date(date);
    today.setHours(0, 0, 0, 0); // Mettez l'heure à minuit pour comparer uniquement les dates
    return dateEncounter < today;
  }
  
  
  

}
