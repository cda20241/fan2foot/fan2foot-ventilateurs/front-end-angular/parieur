import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EncounterPastComponent } from './encounter-past.component';

describe('EncounterPastComponent', () => {
  let component: EncounterPastComponent;
  let fixture: ComponentFixture<EncounterPastComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EncounterPastComponent]
    });
    fixture = TestBed.createComponent(EncounterPastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
