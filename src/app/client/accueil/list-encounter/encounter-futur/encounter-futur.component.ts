import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BetEntorcounterComponent } from 'src/app/client/modals/bet-encounter/bet-encounter.component';
import { Bettor } from 'src/app/models/bettor.model';
import { Encounter, EncounterImpl } from 'src/app/models/encounter.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { EncounterService } from 'src/app/services/encounter.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-encounter-futur',
  templateUrl: './encounter-futur.component.html',
  styleUrls: ['./encounter-futur.component.scss']
})
export class EncounterFuturComponent implements OnInit {
  @Input() bettor!: Bettor;
  encounters: Encounter[] = [];
  teams: Team[] = [];


  constructor(
    private encounterService: EncounterService,
    private teamService: TeamService,
    private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
    //  recuperation des données match
    this.encounterService.getAllEncounter().subscribe((res) => {
      res.forEach((e) => {
        this.encounters.push(new EncounterImpl(e));
      });
      //tri par date du plus ancien au plus récent
      this.encounters.sort((a, b) => {
        let dateA = new Date(a.encounterDate);
        let dateB = new Date(b.encounterDate);
        return dateA.getTime() - dateB.getTime();
      });
    });

    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe((res) => {
      res.forEach((e) => {
        this.teams.push(new TeamImpl(e));
      });
    });
  }
  /**
   * return true si les encounters ne sont pas encore passés
   * @param date
   * @returns
   */
  isFutur(date: Date): boolean {
    let today = new Date();
    let dateEncounter = new Date(date);
    today.setHours(0, 0, 0, 0); // Mettez l'heure à minuit pour comparer uniquement les dates
    return dateEncounter > today;
  }

  betEncounter(encounter: Encounter) {
    const modalRef = this.modalService.open(BetEntorcounterComponent, {
      centered: true,
      size: 'lg',
    });
    modalRef.componentInstance.encounter = encounter;
    modalRef.componentInstance.bettor = this.bettor;
  }




}
