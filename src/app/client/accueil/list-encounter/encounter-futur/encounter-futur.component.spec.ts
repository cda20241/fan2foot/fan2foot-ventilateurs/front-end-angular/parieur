import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EncounterFuturComponent } from './encounter-futur.component';

describe('EncounterFuturComponent', () => {
  let component: EncounterFuturComponent;
  let fixture: ComponentFixture<EncounterFuturComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EncounterFuturComponent]
    });
    fixture = TestBed.createComponent(EncounterFuturComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
