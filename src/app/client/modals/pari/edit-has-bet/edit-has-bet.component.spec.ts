import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHasBetComponent } from './edit-has-bet.component';

describe('EditHasBetComponent', () => {
  let component: EditHasBetComponent;
  let fixture: ComponentFixture<EditHasBetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditHasBetComponent]
    });
    fixture = TestBed.createComponent(EditHasBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
