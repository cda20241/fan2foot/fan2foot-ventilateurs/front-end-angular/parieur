import { Component, Input, OnInit, inject } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BetOn } from 'src/app/enum/beton';
import { EncounterImpl } from 'src/app/models/encounter.model';
import { HasBetImpl } from 'src/app/models/has-bet.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { HasBetService } from 'src/app/services/has-bet.service';
import { TeamService } from 'src/app/services/team.service';
import { BetComponent } from '../../confirmation/bet/bet.component';

@Component({
  selector: 'app-edit-has-bet',
  templateUrl: './edit-has-bet.component.html',
  styleUrls: ['./edit-has-bet.component.scss']
})
export class EditHasBetComponent implements OnInit {
  modal = inject(NgbActiveModal);
  @Input() hasBet!: HasBetImpl;
  @Input() encounter!: EncounterImpl;
  teams!: Team[];
  selectedBet!: Team | null;
  gains!: number;
  betOn: BetOn = BetOn.DRAW;

  constructor(
    private modalService: NgbModal,
    private teamService: TeamService,
    private hasBetService: HasBetService,
  ) {
    this.teams = [];
  }

  ngOnInit(): void {
    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe(res => {
      res.forEach(e => {
        this.teams.push(new TeamImpl(e));
      })
      this.teamSelectBed();
      // calcul des gains
      this.calculGains();
    })
  }

  /**
  * Sélectionne la Team sur laquelle le hasBet a été placé en fonction de la valeur de this.hasBet.betOn.
  * Si le hasBet est sur un match nul, sélectionne null.
  * Si le hasBet est sur la team guest, sélectionne la team guest.
  * Si le hasBet est sur la team host, sélectionne la team host.
  */
  teamSelectBed() {
    switch (this.hasBet.betOn) {
      case "DRAW":
        this.selectBet(null);
        break;
      case "HOST":
        this.selectBet(this.encounter.hostTeam);
        break;
      case "GUEST":
        this.selectBet(this.encounter.guestTeam);
        break;
      default:
        this.selectBet(null);
    }
  }

  /**
 * Sélectionne la Team sur laquelle le hasBet est placé.
 * @param {Team | null} bet - La Team sur laquelle le hasBet est placé. Si le hasBet est sur un match nul, bet est null.
 * Met à jour this.selectedBet et this.betOn en fonction de la valeur de bet.
 * Appelle ensuite la méthode calculGains pour mettre à jour les gains potentiels.
 */
  selectBet(bet: Team | null) {
    this.selectedBet = bet;
    switch (this.selectedBet?.id) {
      case null:
        this.betOn = BetOn.DRAW;
        this.hasBet.betOn = BetOn.DRAW;
        break;
      case this.encounter.guestTeam.id:
        this.betOn = BetOn.GUEST;
        this.hasBet.betOn = BetOn.GUEST;
        break;
      case this.encounter.hostTeam.id:
        this.betOn = BetOn.HOST;
        this.hasBet.betOn = BetOn.HOST;
        break;
      default:
        this.betOn = BetOn.DRAW;
        this.hasBet.betOn = BetOn.DRAW;
    }
    this.calculGains();
  }

  /**
   * Calcule les gains potentiels du bet.
   * Utilise le service HasBetService pour calculer les gains en fonction du amountBet du bet, de la Team sélectionnée et de l'encounter'
   * Met à jour la propriété this.gains avec le résultat.
   */
  calculGains() {
    this.gains = this.hasBetService.calculateGains(this.hasBet.amountBet, this.selectedBet, this.encounter);
  }
  /**
 * Modifie un pari existant et ouvre une modale de confirmation.
 *
 * Cette méthode ferme la modale actuelle et ouvre une nouvelle modale en utilisant le service ModalService. La modale utilise le composant BetConfirmationComponent.
 * Elle passe la rencontre et le pari à modifier au composant de la modale via les propriétés encounter et hasBet de modalRef.componentInstance.
 */
  editHasBet() {
    this.modal.close('Ok click');
    const modalRef = this.modalService.open(BetComponent, {
      centered: true,
      size: 'lg',
    });
    modalRef.componentInstance.encounter = this.encounter;
    modalRef.componentInstance.hasBet = this.hasBet;
  }
}
