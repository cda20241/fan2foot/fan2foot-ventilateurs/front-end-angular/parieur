import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetEncounterComponent } from './bet-encounter.component';

describe('BetEncounterComponent', () => {
  let component: BetEncounterComponent;
  let fixture: ComponentFixture<BetEncounterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BetEncounterComponent]
    });
    fixture = TestBed.createComponent(BetEncounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
