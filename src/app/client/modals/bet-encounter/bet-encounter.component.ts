import { Component, Input, OnInit, inject } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncounterImpl } from 'src/app/models/encounter.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { HasBetService } from 'src/app/services/has-bet.service';
import { TeamService } from 'src/app/services/team.service';
import { BetConfirmationComponent } from '../confirmation/bet-confirmation/bet-confirmation.component';
import { Bettor } from 'src/app/models/bettor.model';
import { Bet } from 'src/app/models/bet.model';
import { BetService } from 'src/app/services/bet.service';
import { BetOn } from 'src/app/enum/beton';

@Component({
  selector: 'app-bet-encounter',
  templateUrl: './bet-encounter.component.html',
  styleUrls: ['./bet-encounter.component.scss']
})
export class BetEntorcounterComponent implements OnInit {
  modal = inject(NgbActiveModal);
  @Input() encounter!: EncounterImpl;
  @Input() amountBet!: number;
  bets: Bet[] = [];
  bet!: Bet;
  betOn: BetOn = BetOn.DRAW;



  gains!: number;
  teams!: Team[];
  @Input() selectedBet!: Team | null;
  @Input() bettor!: Bettor | null;

  constructor(private teamService: TeamService,
    private modalService: NgbModal,
    private hasBetService: HasBetService,
    private betService: BetService
  ) {
    this.teams = [];
  }

  ngOnInit(): void {
    // A voir quel montant minimum d'un pari, ici 1€
    this.amountBet = 1;

    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe(res => {
      res.forEach(e => {
        this.teams.push(new TeamImpl(e))
      })
    });
    // calcul des gains
    this.calculGains();

    // recuperation de la liste des bet:
    this.betService.getAllBets().subscribe(bets => {
      this.bets = bets;
    });


  }
  /* Selectionne le choix du pari (hostTeam, guestTeam ou null */
  selectBet(bet: Team | null) {
    this.selectedBet = bet;
    if (this.selectedBet == null) this.betOn = BetOn.DRAW;
    else if (this.selectedBet.id == this.encounter.guestTeam.id) this.betOn = BetOn.GUEST;
    else this.betOn = BetOn.HOST;
    this.calculGains();
  }

  calculGains() {
    this.gains = this.hasBetService.calculateGains(this.amountBet, this.selectedBet, this.encounter);
  }
  /* au click du bouton parier de la modal bet-encounter, doit ouvrir une autre modal de confirmation */
  parier() {
    this.modal.close('Ok click');
    const modalRef = this.modalService.open(BetConfirmationComponent, {
      centered: true,
      size: 'lg',
    });
    modalRef.componentInstance.encounter = this.encounter;
    modalRef.componentInstance.amountBet = this.amountBet;
    modalRef.componentInstance.betOn = this.betOn;
    modalRef.componentInstance.selectedBet = this.selectedBet;
    modalRef.componentInstance.bettor = this.bettor;
  }

}
