import { Component, Input, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BetOn } from 'src/app/enum/beton';
import { Bettor } from 'src/app/models/bettor.model';
import { EncounterImpl } from 'src/app/models/encounter.model';
import { HasBetImpl } from 'src/app/models/has-bet.model';
import { Team, TeamImpl } from 'src/app/models/team.model';
import { BettorService } from 'src/app/services/bettor.service';
import { HasBetService } from 'src/app/services/has-bet.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.scss']
})
export class BetComponent implements OnInit{
  modal = inject(NgbActiveModal);
  @Input() encounter!: EncounterImpl;
  @Input() hasBet!: HasBetImpl;
  selectedBet!: Team | null;
  betOn: BetOn = BetOn.DRAW;
  gains!: number;
  teams!: Team[];
  bettor!: Bettor;
  /******* a supprimer un fois l'authentification faite */
  idBettor = 3;

  constructor(private router: Router, private hasBetService: HasBetService, private bettorService: BettorService, private teamService: TeamService){
    this.teams = [];
  }

  ngOnInit(): void {
    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe(res => {
      res.forEach(e => {
        this.teams.push(new TeamImpl(e));
      })
      // Sélectionne l'équipe sur laquelle le pari a été placé
      this.teamSelectBed();
      // calcul des gains
      this.calculGains();
    })
    // connexion Bettor
    this.bettorService.getBettor(this.idBettor).subscribe((res) => {
      this.bettor = res;
    });
  }
  /**
 * Sélectionne la Team sur laquelle le hasBet a été placé en fonction de la valeur de this.hasBet.betOn.
 * Si le hasBet est sur un match nul, sélectionne null.
 * Si le hasBet est sur la team guest, sélectionne la team guest.
 * Si le hasBet est sur la team host, sélectionne la team host.
 */
  teamSelectBed(){
    if(this.hasBet.betOn==BetOn.DRAW) this.selectBet(null);
    else if(this.hasBet.betOn==BetOn.GUEST) this.selectBet(this.encounter.guestTeam);
    else if(this.hasBet.betOn==BetOn.HOST) this.selectBet(this.encounter.hostTeam);
  }
/**
 * Sélectionne la Team sur laquelle le hasBet est placé.
 * @param {Team | null} bet - La Team sur laquelle le hasBet est placé. Si le hasBet est sur un match nul, bet est null.
 * Met à jour this.selectedBet et this.betOn en fonction de la valeur de bet.
 * Appelle ensuite la méthode calculGains pour mettre à jour les gains potentiels.
 */
  selectBet(bet: Team | null) {
    this.selectedBet = bet;
    if(this.selectedBet==null) this.betOn = BetOn.DRAW;
    else if(this.selectedBet.id==this.encounter.guestTeam.id) this.betOn = BetOn.GUEST;
    else  this.betOn = BetOn.HOST;
    this.calculGains();
  }
  /**
   * Modifie le bet existant en utilisant le service HasBetService.
   * Ferme la modale et met à jour le bet avec les nouvelles valeurs de this.hasBet.
   * En cas de succès, rafraîchit la page.
   * En cas d'erreur, affiche l'erreur dans la console.
   */
  editHasBet(){
    this.modal.close('Ok click');
    this.hasBetService.editHasBet(this.hasBet.id, this.hasBet).subscribe({
      next: () => {
        // Rafraîchir la page
        this.router
          .navigateByUrl('/paris', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/paris']);
          });
      },
      error: (error) => {
        console.error(error);
      },
    });
  }
  /**
 * Calcule les gains potentiels du bet.
 * Utilise le service HasBetService pour calculer les gains en fonction du amountBet du bet, de la Team sélectionnée et de l'encounter'
 * Met à jour la propriété this.gains avec le résultat.
 */
  calculGains(){
    console.log(this.encounter)
    this.gains=this.hasBetService.calculateGains(this.hasBet.amountBet, this.selectedBet, this.encounter);
  }

}
