import { Component, Input, OnInit, inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EncounterImpl } from 'src/app/models/encounter.model';
import { Team } from 'src/app/models/team.model';
import { BetService } from '../../../../services/bet.service';
import { HasBet } from '../../../../models/has-bet.model';
import { Router } from '@angular/router';
import { HasBetService } from 'src/app/services/has-bet.service';
import { Bettor } from 'src/app/models/bettor.model';
import { Bet } from 'src/app/models/bet.model';
import { BettorService } from '../../../../services/bettor.service';
import { BetOn } from 'src/app/enum/beton';

@Component({
  selector: 'app-bet-confirmation',
  templateUrl: './bet-confirmation.component.html',
  styleUrls: ['./bet-confirmation.component.scss']
})
export class BetConfirmationComponent implements OnInit {

  modal = inject(NgbActiveModal);
  @Input() encounter!: EncounterImpl;
  @Input() amountBet!: number;
  @Input() betOn!: BetOn;
  @Input() bettor!: Bettor;
  @Input() selectedBet!: Team | null;
  newHasBet!: HasBet;
  gains!: number;

  bets: Bet[] = [];
  bet!: Bet;

  constructor(
    private hasBetService: HasBetService,
    private betService: BetService,
    private bettorService: BettorService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    // recuperation de tous les bets
    this.betService.getAllBets().subscribe(bets => {
      this.bets = bets;
      //recupération du bet en fonction de l'idEncounter
      let betFound: Bet | undefined = this.bets.find(bet => bet.idEncounter === this.encounter.id);
      if (betFound) {
        this.bet = betFound;
      } else {
        console.log('erreur bet non trouvé, idEncounter: ' + this.encounter.id);
        this.modal.dismiss();
      }
      // calcul des gains
      this.calculGains();
    });

  }

  calculGains() {
    this.gains = this.hasBetService.calculateGains(this.amountBet, this.selectedBet, this.encounter);
  }

  /** envoi du nouveau bet au back** */
  createHasBet() {
    this.modal.close('Ok click');
    this.createNewHasBet();
    this.hasBetService.createHasBet(this.newHasBet).subscribe({
      next: () => {
        /* fonction pour enlever le montant du pari de la balance utilisateur */
        this.bettor.balance = this.bettor.balance - this.amountBet;
        this.bettorService.editBettor(this.bettor.id, this.bettor);
        // Rafraîchir la page
        this.router
          .navigateByUrl('/paris', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/paris']);
          });
      },
      error: (error) => {
        console.error(error);
      },
    });
  }
  /** Création du nouveau bet */
  createNewHasBet() {
    this.newHasBet = {
      id: 0,
      bet: this.bet,
      bettor: this.bettor,
      betOn: this.betOn,
      amountBet: this.amountBet,
      paid: false,
    };
  }

}
