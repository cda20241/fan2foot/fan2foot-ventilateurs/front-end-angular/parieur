import { Component, Input, inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { Bettor, BettorImpl } from 'src/app/models/bettor.model';
import { Encounter, EncounterImpl } from 'src/app/models/encounter.model';
import { HasBet, HasBetImpl } from 'src/app/models/has-bet.model';
import { Team } from 'src/app/models/team.model';
import { BettorService } from 'src/app/services/bettor.service';
import { EncounterService } from 'src/app/services/encounter.service';
import { HasBetService } from 'src/app/services/has-bet.service';

@Component({
  selector: 'app-bet-delete',
  templateUrl: './bet-delete.component.html',
  styleUrls: ['./bet-delete.component.scss']
})
export class BetDeleteComponent {
  modal = inject(NgbActiveModal);
  encounter!: Encounter;
  gagnant!: String | Team;
  @Input() hasBet!: HasBetImpl;
  bettor!: Bettor;

  private destroy$ = new Subject<void>();

  // a supprimer une fois l'identification intégrée
  idUser: number = 3;



  constructor(
    private bettorService: BettorService,
    private encounterService: EncounterService,
    private hasBetService: HasBetService,
  ) { }

  ngOnInit(): void {
    this.encounterService.getOneEncounter(this.hasBet.bet.idEncounter).subscribe(
      (encounter: Encounter) => {
        this.encounter = encounter;
        switch (this.hasBet.betOn) {
          case "DRAW":
            this.gagnant = "Match nul";
            break;
          case "HOST":
            this.gagnant = this.encounter.hostTeam.name;
            break;
          case "GUEST":
            this.gagnant = this.encounter.guestTeam.name;
            break;
          default:
            this.gagnant = "Match nul";
        }
      }
    ),

      // connexion parieur 
      this.bettorService.getBettor(this.idUser).subscribe((res) => {
        this.bettor = res;
      });
  }


  deleteHasBet(): void {
    this.hasBetService.deleteHasBet(this.hasBet.id);
    this.modal.close('Ok click');
  }

}
