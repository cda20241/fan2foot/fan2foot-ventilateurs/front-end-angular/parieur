import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetDeleteComponent } from './bet-delete.component';

describe('BetDeleteComponent', () => {
  let component: BetDeleteComponent;
  let fixture: ComponentFixture<BetDeleteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BetDeleteComponent]
    });
    fixture = TestBed.createComponent(BetDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
