import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.scss']
})
export class AppFooterComponent {

  constructor(public router:Router){}

}
