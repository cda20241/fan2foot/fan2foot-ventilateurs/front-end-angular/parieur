import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-paris',
  templateUrl: './paris.component.html',
  styleUrls: ['./paris.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ParisComponent {
  showBet: boolean = true;
  activeButton: string = 'encours';

  // permet de passer de match passés a match futur
  toggleEncours() {
    this.showBet = false;
  }

  togglePasse() {
    this.showBet = true;
  }
  setActive(button: string) {
    this.activeButton = button;
  }
}
