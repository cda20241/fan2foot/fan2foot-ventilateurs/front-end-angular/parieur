import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Bettor } from 'src/app/models/bettor.model';
import { Encounter } from 'src/app/models/encounter.model';
import { HasBetWithEncounter } from 'src/app/models/has-bet-with-encounter';
import { HasBet } from 'src/app/models/has-bet.model';
import { Team } from 'src/app/models/team.model';
import { BettorService } from 'src/app/services/bettor.service';
import { EncounterService } from 'src/app/services/encounter.service';
import { HasBetService } from 'src/app/services/has-bet.service';

@Component({
  selector: 'app-bet-passe',
  templateUrl: './bet-passe.component.html',
  styleUrls: ['./bet-passe.component.scss']
})
export class BetPasseComponent implements OnInit {
  dataLoaded = false;
  bettor!: Bettor;
  hasBets: HasBet[] = [];
  encounters: Encounter[] = [];
  hasBetsWithEncounters: HasBetWithEncounter[] = [];
  // a supprimer une fois l'identification intégrée
  idBettor: number = 3;

  constructor(
    private bettorService: BettorService,
    private hasBetService: HasBetService,
    private encounterService: EncounterService,
  ) { }

  ngOnInit(): void {
    // connexion Bettor
    this.bettorService.getBettor(this.idBettor).subscribe((res) => {
      this.bettor = res;
      this.getBettorHasBets(this.bettor);
    });
  }
  /**
  * Calcule les gains potentiels d'un bet.
  * 
  * @param {number} amountBet - Le montant du bet.
  * @param {Team | null} selectedBet - L'équipe sur laquelle le bet est placé. Si le bet est sur un match nul, selectedBet est null.
  * @param {Encounter} encounter - L'encounter' sur laquelle le bet est placé.
  * 
  * @returns Les gains potentiels du pari, calculés en utilisant le service HasBetService.
  */
  calculGains(amountBet: number, selectedBet: Team | null, encounter: Encounter) {
    return this.hasBetService.calculateGains(amountBet, selectedBet, encounter);
  }
  /**
   * Récupère tous les bet associés à un bettor spécifique.
   * 
   * @param {Bettor} bettor - Le bettor pour lequel récupérer les bets.
   * 
   * Cette méthode s'abonne au service HasBetService pour récupérer tous les bets.
   * Elle filtre la liste des bets pour ne conserver que ceux associés à l'ID du bettor spécifié.
   * Les bets filtrés sont stockés dans la propriété this.hasBets.
   * 
   * Ensuite, pour chaque bet, elle récupère l'encounter associée en utilisant le service EncounterService.
   * Les encounters sont stockées dans la propriété this.hasBetsWithEncounters.
   * 
   * Une fois que toutes les encounters sont récupérées, elle définit la propriété this.dataLoaded sur true.
   */
  getBettorHasBets(bettor: Bettor): void {
    this.hasBetService.getAllHasBets().subscribe(hasBets => {
      // recuperation des hasbets corespondants a l'idBettor
      this.hasBets = hasBets.filter(hasBet => hasBet.bettor.id === bettor.id);
      const encounterObservables = this.hasBets.map(hasBet =>
        this.encounterService.getOneEncounter(hasBet.bet.idEncounter)
      );
      /** pour regler l'async */
      forkJoin(encounterObservables).subscribe(encounters => {
        encounters.forEach((encounter, index) => {
          this.hasBetsWithEncounters.push({ hasBet: this.hasBets[index], encounter });
        });
        this.dataLoaded = true;
      });
    });
  }
}