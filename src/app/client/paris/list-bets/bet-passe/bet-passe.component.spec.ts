import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetPasseComponent } from './bet-passe.component';

describe('BetPasseComponent', () => {
  let component: BetPasseComponent;
  let fixture: ComponentFixture<BetPasseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BetPasseComponent]
    });
    fixture = TestBed.createComponent(BetPasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
