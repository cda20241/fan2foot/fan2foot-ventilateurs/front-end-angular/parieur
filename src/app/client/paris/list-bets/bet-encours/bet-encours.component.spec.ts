import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetEncoursComponent } from './bet-encours.component';

describe('BetEncoursComponent', () => {
  let component: BetEncoursComponent;
  let fixture: ComponentFixture<BetEncoursComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BetEncoursComponent]
    });
    fixture = TestBed.createComponent(BetEncoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
