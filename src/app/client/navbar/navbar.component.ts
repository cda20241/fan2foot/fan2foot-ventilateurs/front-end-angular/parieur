import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Bettor } from 'src/app/models/bettor.model';
import { BettorService } from 'src/app/services/bettor.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  // A SUPPRIMER UNE FOIS L'IDENTIFICATION FAITE
  idBettor: number = 3;
  bettor$!: Observable<Bettor>;

  constructor(
    private bettorService: BettorService,
    public router: Router
  ) {
  }

  ngOnInit(): void {
    // connexion parieur
    this.bettor$ = this.bettorService.getBettor(this.idBettor);

  }
}
