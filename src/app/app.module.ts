import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { NavbarComponent } from './client/navbar/navbar.component';
import { AccueilComponent } from './client/accueil/accueil.component';
import { ParisComponent } from './client/paris/paris.component';
import { provideHttpClient, withFetch } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AbonnementsComponent } from './client/abonnements/abonnements.component';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { BetEncoursComponent } from './client/paris/list-bets/bet-encours/bet-encours.component';
import { BetPasseComponent } from './client/paris/list-bets/bet-passe/bet-passe.component';
import { EncounterPastComponent } from './client/accueil/list-encounter/encounter-past/encounter-past.component';
import { EncounterFuturComponent } from './client/accueil/list-encounter/encounter-futur/encounter-futur.component';
import { CommonModule } from '@angular/common';
import { BetEntorcounterComponent } from './client/modals/bet-encounter/bet-encounter.component';
import { FormsModule } from "@angular/forms";
import { FilterPaidHasBetPipe } from './pipes/filter-paid-has-bet-pipe.pipe';
import { EditHasBetComponent } from './client/modals/pari/edit-has-bet/edit-has-bet.component';
import { HeaderComponent } from './client/header/header.component';
import { BetConfirmationComponent } from './client/modals/confirmation/bet-confirmation/bet-confirmation.component';
import { AppFooterComponent } from './client/app-footer/app-footer.component';
import { LoginComponent } from './login/login.component';
import { BetComponent } from "./client/modals/confirmation/bet/bet.component";
import { BetDeleteComponent } from './client/modals/confirmation/bet-delete/bet-delete.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    AccueilComponent,
    ParisComponent,
    AbonnementsComponent,
    ParisComponent,
    BetEncoursComponent,
    BetPasseComponent,
    EncounterPastComponent,
    EncounterFuturComponent,
    BetEntorcounterComponent,
    BetConfirmationComponent,
    FilterPaidHasBetPipe,
    EditHasBetComponent,
    LoginComponent,
    AppFooterComponent,
    BetComponent,
    BetConfirmationComponent,
    AppFooterComponent,
    FilterPaidHasBetPipe,
    EditHasBetComponent,
    BetComponent,
    BetConfirmationComponent,
    BetDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbNavModule,
    CommonModule,
    FormsModule
  ],
  providers: [provideHttpClient(withFetch())],
  bootstrap: [AppComponent]
})
export class AppModule { }
