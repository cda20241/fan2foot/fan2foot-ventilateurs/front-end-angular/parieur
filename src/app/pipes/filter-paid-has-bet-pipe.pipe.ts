import { Pipe, PipeTransform } from '@angular/core';
import { HasBetWithEncounter } from '../models/has-bet-with-encounter';

@Pipe({
  name: 'filterPaidHasBetPipe'
})
/** trie les hasbets en fonction si le hasbet.paid vaut true ou false */
export class FilterPaidHasBetPipe implements PipeTransform {
  transform(hasBetsWithEncounters: any[], paid: boolean): any[] {
    const filtered = hasBetsWithEncounters.filter(item => item.hasBet.paid === paid);;
    return filtered;
  }

}
