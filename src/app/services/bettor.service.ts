import { Injectable } from '@angular/core';
import { env } from '../env/env';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bettor } from '../models/bettor.model';

@Injectable({
  providedIn: 'root'
})
export class BettorService {
  urlBettor = env.apiBettor;

  constructor(private http: HttpClient) { }

  getBettor(id: number): Observable<Bettor> {
    return this.http.get<Bettor>(this.urlBettor + 'bettor/' + id);
  }

  editBettor(id: number, bettor: Bettor): Observable<Bettor> {
    return this.http.put<Bettor>(this.urlBettor + 'bettor/' + id, bettor);
  }



}
