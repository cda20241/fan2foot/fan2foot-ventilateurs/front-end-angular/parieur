import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, of } from 'rxjs';
import { env } from '../env/env';
import { Team, TeamImpl } from '../models/team.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  urlManager = env.apiManager;

  constructor(private http: HttpClient) { }

  createTeam(team: Team): Observable<Team> {
    console.log(team)
    return this.http.post<Team>(this.urlManager + 'teams', team);
  }



  getOneTeam(id: number): Observable<Team> {
    return this.http.get<Team>(this.urlManager + 'teams/' + id);
  }

  getAllTeam(): Observable<Team[]> {
    return this.http.get<Team[]>(this.urlManager + 'teams');
  }

  deleteTeam(id: number): Observable<Team> {
    return this.http.delete<Team>(this.urlManager + id);
  }


  searchTeamList(term: string): Observable<Team[]> {
    if (term.trim()) { // Si le terme de recherche n'est pas vide
      return this.getAllTeam().pipe(
        map(teams => teams.filter(team => team.name.toLowerCase().includes(term.toLowerCase())))
      );
    } else {
      return this.getAllTeam();
    }
  }


}

