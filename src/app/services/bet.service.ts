import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { env } from '../env/env';
import { HttpClient } from '@angular/common/http';
import { Bet } from '../models/bet.model';

@Injectable({
  providedIn: 'root'
})
export class BetService {
  urlBettor = env.apiBettor;

  constructor(private http: HttpClient) { }

  /* Methode provisoire pour le calcul des gains */

  getAllBets(): Observable<Bet[]> {
    return this.http.get<Bet[]>(this.urlBettor + 'bets');
  }

}
