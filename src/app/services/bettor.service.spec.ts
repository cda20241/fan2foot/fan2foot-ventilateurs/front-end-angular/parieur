import { TestBed } from '@angular/core/testing';

import { BettorService } from './bettor.service';

describe('BettorService', () => {
  let service: BettorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BettorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
