import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { env } from '../env/env';
import { Encounter } from '../models/encounter.model';


@Injectable({
  providedIn: 'root',
})
export class EncounterService {
  urlManager = env.apiManager;

  constructor(private http: HttpClient) { }

  createEncounter(encounter: Encounter): Observable<Encounter> {
    return this.http.post<Encounter>(this.urlManager + 'encounters', encounter);
  }

  editEncounter(id: number, encounter: Encounter): Observable<Encounter> {
    return this.http.put<Encounter>(this.urlManager + 'encounter/' + id, encounter);
  }

  getOneEncounter(id: number): Observable<Encounter> {
    return this.http.get<Encounter>(this.urlManager + 'encounter/' + id);
  }

  getAllEncounter(): Observable<Encounter[]> {
    return this.http.get<Encounter[]>(this.urlManager + 'encounters');
  }

  deleteEncounter(id: number): Observable<string> {
    return this.http.delete<string>(this.urlManager + 'encounter/' + id);
  }

  endEncounter(
    id: number,
    resultatHost: number,
    resultatGuest: number
  ): Observable<Encounter> {
    let resultat = {
      resultatHost: resultatHost,
      resultatGuest: resultatGuest,
    };
    return this.http.post<Encounter>(this.urlManager + 'close/' + id, resultat);
  }



}
