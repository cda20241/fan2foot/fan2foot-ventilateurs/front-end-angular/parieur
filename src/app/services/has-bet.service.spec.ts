import { TestBed } from '@angular/core/testing';

import { HasBetService } from './has-bet.service';

describe('HasBetService', () => {
  let service: HasBetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HasBetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
