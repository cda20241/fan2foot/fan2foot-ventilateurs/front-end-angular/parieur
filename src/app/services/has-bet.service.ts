import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { env } from '../env/env';
import { Team } from '../models/team.model';
import { HasBet } from '../models/has-bet.model';
import { Encounter } from '../models/encounter.model';

@Injectable({
  providedIn: 'root'
})
export class HasBetService {
  urlBettor = env.apiBettor;

  constructor(private http: HttpClient) { }
  /**
 * Calcule les gains potentiels d'un bet.
 *
 * @param {number} amountBet - Le montant du bet.
 * @param {Team | null} selectedBet - La team sur laquelle le bet est placé. Si le bet est sur un match nul, selectedBet est null.
 * @param {Encounter} encounter - L'encounter' sur laquelle le bet est placé.
 *
 * @returns Les gains potentiels du bet. Si une team est sélectionnée, les gains sont calculés en multipliant le montant du bet par le ratioRate de la team.
 *          Si le bet est sur un match nul, les gains sont calculés en prenant la moyenne des ratios des deux teams.
 */
  calculateGains(amountBet: number, selectedBet: Team | null, encounter: Encounter) {
    let gains = 0;
    if (selectedBet != null) {
      gains = amountBet * selectedBet.ratioRate; // a revoir pour les calculs de gains
    } else {
      gains = amountBet * (encounter.guestTeam.ratioRate + encounter.hostTeam.ratioRate) / 2; // ratio a voir pour match nul
    }
    return gains;
  }
  /**
 * Crée un nouveau bet.
 *
 * @param {HasBet} hasBet - Le bet à créer.
 *
 * @returns Un Observable qui émet le bet créé. Le bet est créé en envoyant une requête POST à l'URL spécifiée.
 */
  createHasBet(hasBet: HasBet): Observable<HasBet> {
    return this.http.post<HasBet>(this.urlBettor + 'hasbet', hasBet);
  }
  /**
   * Récupère tous les bets.
   *
   * @returns Un Observable qui émet un tableau de tous les bets. Les bets sont récupérés en envoyant une requête GET à l'URL spécifiée.
   */
  getAllHasBets(): Observable<HasBet[]> {
    return this.http.get<HasBet[]>(this.urlBettor + 'hasbets');
  }
  /**
   * Modifie un bet existant.
   *
   * @param {number} id - L'ID du bet à modifier.
   * @param {HasBet} encounter - Les nouvelles données du bet.
   *
   * @returns Un Observable qui émet le bet modifié. Le bet est modifié en envoyant une requête PUT à l'URL spécifiée.
   */
  editHasBet(id: number, encounter: HasBet): Observable<HasBet> {
    return this.http.put<HasBet>(this.urlBettor + 'hasbet/' + id, encounter);
  }
  getHasBet(id: number): Observable<HasBet> {
    return this.http.get<HasBet>(this.urlBettor + 'hasbet/' + id);
  }

  deleteHasBet(id: number): Observable<HasBet> {
    return this.http.delete<HasBet>(this.urlBettor + 'hasbet/' + id);
  }





}
