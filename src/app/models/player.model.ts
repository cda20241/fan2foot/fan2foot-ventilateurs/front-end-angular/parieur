import { Goal } from './goal.model';
import { Team, TeamImpl } from './team.model';

export interface Player {
  id: number;
  firstName: string;
  lastName: string;
  team: Team;
  urlPicture: string;
  goals: Goal[];
}

export interface PlayerImpl extends Player {}

export class PlayerImpl {
  constructor(player?: Player) {
    this.id = player?.id ?? 0;
    this.firstName = player?.firstName ?? 'unknown';
    this.lastName = player?.lastName ?? 'unknown';
    this.team = player?.team ?? new TeamImpl();
    this.urlPicture = player?.urlPicture ?? '../../assets/player-default.svg';
    this.goals = player?.goals ?? [];
  }
}

export interface PlayerExt extends Player {}

export class PlayerExt {}

