import { Encounter, EncounterImpl } from './encounter.model';
import { Player, PlayerImpl } from './player.model';

export interface Goal {
  id: number;
  encounter: Encounter;
  player: Player;
}

export interface GoalImpl extends Goal {}

export class GoalImpl {
  constructor(goal?: Goal) {
    this.id = goal?.id ?? 0;
    this.encounter = goal?.encounter ?? new EncounterImpl();
    this.player = goal?.player ?? new PlayerImpl();
  }
}

export interface GoalExt extends Goal {}

export class GoalExt {}
