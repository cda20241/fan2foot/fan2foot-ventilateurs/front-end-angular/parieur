import { HasBet } from "./has-bet.model";
import { Player } from "./player.model";


export interface Bettor {
  id: number,
  balance: number,
  subscribedPlayers: Player[] | null
}

export interface BettorImpl extends Bettor { }

export class BettorImpl {
  constructor(bettor?: Bettor) {
    this.id = bettor?.id ?? 0;
    this.balance = bettor?.balance ?? 0;
    this.subscribedPlayers = bettor?.subscribedPlayers ?? null;

  }
}
