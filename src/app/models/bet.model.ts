import { BetOn } from "../enum/beton";
import { HasBet, HasBetImpl } from "./has-bet.model";


export interface Bet {
    id: number,
  idEncounter : number,
  hasBets: HasBet[],
  }

  export interface BetImpl extends Bet{}

  export class BetImpl {
    constructor(bet?: Bet) {
      this.id = bet?.id ?? 0;
      this.idEncounter = bet?.idEncounter ?? 0;
      this.hasBets = bet?.hasBets ?? [] ;
    }
  }
