import { BetOn } from "../enum/beton";
import { Bet, BetImpl } from "./bet.model";
import { Bettor, BettorImpl } from "./bettor.model";


 export interface HasBet {
    id: number,
    amountBet: number,
    betOn: BetOn,
    bet: Bet,
    bettor: Bettor,
    paid: boolean
  }
  
  export interface HasBetImpl extends HasBet { }
  
  export class HasBetImpl {
    constructor(hasBet?: HasBet) {
      this.id = hasBet?.id ?? 0;
      this.amountBet = hasBet?.amountBet ?? 0;
      this.betOn = hasBet?.betOn ?? BetOn.DRAW;
      this.bet = hasBet?.bet ?? new BetImpl;
      this.bettor = hasBet?.bettor ?? new BettorImpl;
      this.paid = hasBet?.paid ?? false;
    }
  }