

export interface Team {
  id: number;
  name: string;
  country: string; //enum contry
  //: string;
  //players: Player[] | null;
  //encounters: Encounter[] | null;
  ratioRate: number;
}

export interface TeamImpl extends Team { }

export class TeamImpl {
  constructor(team?: Team) {
    this.id = team?.id ?? 0;
    this.name = team?.name ?? '';
    this.country = team?.country ?? '';
    //this.urlPicture = team?.urlPicture ?? ''; // urlpicture default
    //this.players = team?.players ?? null;
    //this.encounters = team?.encounters ?? null;
    this.ratioRate = team?.ratioRate ?? 1.0;
  }
}
