import { Team, TeamImpl } from "./team.model";

export interface Encounter {
  id: number,
  hostTeam: Team,
  guestTeam: Team,
  hostResult: number | null,
  guestResult: number | null,
  encounterDate: Date
}

export interface EncounterImpl extends Encounter { }

export class EncounterImpl {
  constructor(encounter?: Encounter) {
    this.id = encounter?.id ?? 0;
    this.hostTeam = encounter?.hostTeam ?? new TeamImpl;
    this.guestTeam = encounter?.guestTeam ?? new TeamImpl;
    this.hostResult = encounter?.hostResult ?? null;
    this.guestResult = encounter?.guestResult ?? null;
    this.encounterDate = encounter?.encounterDate ? new Date(encounter.encounterDate) : new Date;
  }
}
