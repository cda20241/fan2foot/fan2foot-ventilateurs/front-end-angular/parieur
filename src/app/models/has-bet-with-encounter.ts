import { Encounter } from "./encounter.model";
import { HasBet } from "./has-bet.model";

export interface HasBetWithEncounter {
    hasBet: HasBet;
    encounter: Encounter;
}