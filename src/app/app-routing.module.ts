import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './client/accueil/accueil.component';
import { ParisComponent } from './client/paris/paris.component';
import { AbonnementsComponent } from './client/abonnements/abonnements.component';
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'accueil', component: AccueilComponent },
  { path: 'paris', component: ParisComponent },
  { path: 'abonnements', component: AbonnementsComponent },
  { path: 'login', component: LoginComponent },


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

