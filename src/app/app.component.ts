import { Component } from '@angular/core';
import { Encounter, EncounterImpl } from './models/encounter.model';
import { Team, TeamImpl } from './models/team.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EncounterService } from './services/encounter.service';
import { TeamService } from './services/team.service';
import { BetEntorcounterComponent } from './client/modals/bet-encounter/bet-encounter.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'parieur';
  encounters: Encounter[] = [];
  teams: Team[] = [];

  constructor(
    private encounterService: EncounterService,
    private teamService: TeamService,
    private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
    //  recuperation des données match
    this.encounterService.getAllEncounter().subscribe((res) => {
      res.forEach((e) => {
        this.encounters.push(new EncounterImpl(e));
      });
      //tri par date du plus ancien au plus récent
      this.encounters.sort((a, b) => {
        let dateA = new Date(a.encounterDate);
        let dateB = new Date(b.encounterDate);
        return dateA.getTime() - dateB.getTime();
      });
    });

    //  recuperation des données équipe
    this.teamService.getAllTeam().subscribe((res) => {
      res.forEach((e) => {
        this.teams.push(new TeamImpl(e));
      });
    });
  }


  betEncounter(encounter: Encounter) {
    const modalRef = this.modalService.open(BetEntorcounterComponent, {
      centered: true,
      size: 'lg',
    });
    modalRef.componentInstance.encounter = encounter;
  }



}

